import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VehicleInfo, RealTimeData, Vehicle } from '../interface/vehicle-data';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VehicleDataService {

  constructor(
    private http: HttpClient
  ) { }

  // baseUrl = 'http://localhost:8080/api/v1';
  baseUrl = 'https://easy-fleet-service-awhixk03-dev.apps.sandbox-m3.1530.p1.openshiftapps.com/api/v1';

  allVehicleInfoEndpoint = '/static/vehicles';
  vehicleInfoByDriverEndpoint = '/static/vehicle/driver';
  rtdEndpoint = '/real-time/vehicle';
  tradeEndpoint = '/static/vehicle/trade';

  aiEndpoint = '/ai';

  fetchAllVehicleInfo(): Observable<VehicleInfo[]> {
    return this.makeNoArgsRequest(this.allVehicleInfoEndpoint);
  }

  fetchVehicleByDriver(id: number): Observable<VehicleInfo> {
    return this.makeOneArgRequest(this.vehicleInfoByDriverEndpoint, id, 'id');
  }

  fetchRTD(id: number): Observable<RealTimeData> {
    return this.makeOneArgRequest(this.rtdEndpoint, id, 'id');
  }

  tradeAssignments(vehicleOne: VehicleInfo, vehicleTwo: VehicleInfo) {
    return this.makeTwoArgsRequest(this.tradeEndpoint, vehicleOne, vehicleTwo);
  }

  fetchAiInsights(vehicle: Vehicle, rtd: RealTimeData): Observable<any> {
    return this.makeOneArgRequestTwoParams(this.aiEndpoint, vehicle.vehicleId, 'id', rtd.groupUuid, 'uuid');
  }

  private makeNoArgsRequest(endpoint: string): Observable<any[]> {
    const headers = new HttpHeaders();
    return this.http.get<VehicleInfo[]>(this.baseUrl + endpoint, { headers });
  }

  private makeOneArgRequest(endpoint: string, argument: any, paramName: string): Observable<any> {
    const headers = new HttpHeaders();
    const params = new HttpParams().append(paramName, argument);
    return this.http.get<any>(this.baseUrl + endpoint, { headers, params });
  }

  private makeOneArgRequestTwoParams(endpoint: string, argumentOne: any, paramOneName: string, argumentTwo: any, paramTwoName: string): Observable<any> {
    const headers = new HttpHeaders();
    const params = new HttpParams().append(paramOneName, argumentOne).append(paramTwoName, argumentTwo);
    return this.http.get<any>(this.baseUrl + endpoint, { headers, params });
  }

  private makeTwoArgsRequest(endpoint: string, vehicleOne: VehicleInfo, vehicleTwo: VehicleInfo): Observable<any> {
    const headers = new HttpHeaders();

    const body: VehicleInfo[] = [];
    body.push(vehicleOne);
    body.push(vehicleTwo);

    return this.http.put<any>(this.baseUrl + endpoint, body, { headers });
  }
}
