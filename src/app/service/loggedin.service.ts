import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoggedinService {
  private readonly LOGGED_IN_STORAGE_KEY = 'demoToggleValue';
  private toggleValueSubject = new BehaviorSubject<boolean>(this.getInitialBooleanValue());
  toggleValue$ = this.toggleValueSubject.asObservable();

  private readonly USER_STORAGE_KEY = 'userValue';
  private userValueSubject = new BehaviorSubject<number>(this.getInitialNumberValue());
  userValue$ = this.userValueSubject.asObservable();

  constructor() {}

  setToggleValue(value: boolean): void {
    this.toggleValueSubject.next(value);
    sessionStorage.setItem(this.LOGGED_IN_STORAGE_KEY, JSON.stringify(value));

    if (!value) {
      // Default user is 0 (admin). If not logged in, no user to impersonate.
      this.setUserValue(0);
    }
  }

  setUserValue(value: number): void {
    this.userValueSubject.next(value); // If user is 0, admin, else, impersonate according to driver ID.
    sessionStorage.setItem(this.USER_STORAGE_KEY, JSON.stringify(value));
  }

  private getInitialBooleanValue(): boolean {
    const storedValue = sessionStorage.getItem(this.LOGGED_IN_STORAGE_KEY);
    return storedValue ? JSON.parse(storedValue) : false;
  }

  private getInitialNumberValue(): number {
    const storedValue = sessionStorage.getItem(this.USER_STORAGE_KEY);
    return storedValue ? JSON.parse(storedValue) : 0;
  }
}

