export interface VehicleInfo {
    vehicle:  Vehicle;
    driver: Driver;
}

export interface Driver {
    driverId:  number;
    firstName: string;
    lastName:  string;
}

export interface DriverWithId {
    vehicleId: number;
    driverId:  number;
    firstName: string;
    lastName:  string;
}

export interface Vehicle {
    vehicleId:         number;
    model:             string;
    yearOfManufacture: number;
    manufacturer:      string;
    batteryCapacity:   string;
    mileage:           number;
    vin:               string;
    licensePlate:      string;
    state:             string;
}

export interface RealTimeData {
    batteryStatus:             number;
    currentMileage:            number;
    motorHealth:               string;
    errorCodes:                any[];
    brakePadCondition:         number;
    brakeFluidLevel:           number;
    tirePressureLow:           boolean;
    airConditioningFunctional: boolean;
    softwareUpToDate:          boolean;
    timestamp:                 Date;
    groupUuid:                 string;
}
