import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { Router } from '@angular/router';
import { LoggedinService } from '../service/loggedin.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  positionOptions: TooltipPosition[] = ['below', 'above', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  isLoggedIn!: boolean;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private _snackBar: MatSnackBar,
    private router: Router,
    public loggedInService: LoggedinService
  ) {
    this.loggedInService.toggleValue$.subscribe((value: boolean) => this.isLoggedIn = value);
  }

  logout() {
    this.loggedInService.setToggleValue(false);
    this.router.navigate(['/home']);
  }

  copyToClipboard() {
    navigator.clipboard.writeText("https://event-lens-web.vercel.app/home");
    this._snackBar.open("Link copied to clipboard!", "Dismiss", {
      duration: 3000
    });
  }

  onToggleChange(event: any) {
    this.loggedInService.setToggleValue(this.isLoggedIn);
  }
}
