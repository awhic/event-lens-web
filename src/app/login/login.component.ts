import { Component, inject } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';
import { LoggedinService } from '../service/loggedin.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  private fb = inject(FormBuilder);
  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });

  hide = true;
  isIncorrect = false;

  constructor(
    private loggedInService: LoggedinService,
    private router: Router
  ) { }

  /**
    * Handles the submission of the login form. If the entered credentials match the mock admin credentials,
    * the user is considered authenticated and is navigated to the dashboard. If the credentials do not match,
    * the form is marked as incorrect.
    *
    * Mock admin credentials:
    * - Username: admin
    * - Password: admin
    *
    * Authentication is handled via session storage, and the mock authentication state is updated accordingly.
    */
  onSubmit(): void {
    if (this.loginForm?.get('username')?.value === 'admin' && this.loginForm?.get('password')?.value === 'admin') {
      this.isIncorrect = false;
      this.loggedInService.setToggleValue(true);
      this.router.navigate(['/dashboard']);
    } else {
      this.isIncorrect = true;
    }
  }
}
