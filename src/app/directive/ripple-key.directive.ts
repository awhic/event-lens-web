import { Directive, Input } from '@angular/core';
import { MatRipple } from '@angular/material/core';

@Directive({
  selector: '[rippleKey]'
})
export class RippleKeyDirective {
  @Input('rippleKey') key!: string;

  constructor(private ripple: MatRipple) {}

  /**
    * A custom directive grants the ability to designate specific ripple effects.
    * This allows the dashboard component to know which MatRipple to animate when an RTD value changes.
    * 
    * @returns {MatRipple} The instance of MatRipple associated with the component.
    */
  getRippleInstance(): MatRipple {
    return this.ripple;
  }
}