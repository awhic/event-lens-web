import { AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription, interval, startWith, switchMap } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DriverWithId, Vehicle, VehicleInfo, RealTimeData } from '../interface/vehicle-data';
import { VehicleDataService } from '../service/vehicle-data.service';
import { MatExpansionPanel } from '@angular/material/expansion';
import { LoggedinService } from '../service/loggedin.service';
import { MatRipple } from '@angular/material/core';
import { RippleKeyDirective } from '../directive/ripple-key.directive';
import { KeyValue } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {

  localTime!: string;
  utcTime!: string;
  timerId: number | null = null;

  user!: number;
  updatedTime!: string;

  vehicleStaticDataSub!: Subscription;
  vehicleRealTimeDataSub!: Subscription;
  tradeSub!: Subscription;
  aiSub!: Subscription;

  staticData: Vehicle[] = [];
  realTimeData: RealTimeData[] = [];

  dataSource!: MatTableDataSource<Vehicle>;
  vehicleInfo!: VehicleInfo[];

  columnsToDisplay = [
    'ID',
    'Year',
    'Model',
    'Manufacturer',
    'VIN',
    'License',
    'State',
    'Battery Capacity'
  ];
  columnProperties = [
    'vehicleId',
    'yearOfManufacture',
    'model',
    'manufacturer',
    'vin',
    'licensePlate',
    'state',
    'batteryCapacity'
  ];
  columnsMap!: Map<string, string>;
  columnsToDisplayWithExpand!: string[];

  detailData!: any;
  detailHeaders = [
    'Battery Status',
    'Current Mileage',
    'Motor Health',
    'Brake Pad Condition',
    'Brake Fluid Level',
    'Tire Pressure',
    'A/C Functional',
    'Onboard Software',
    'Data Timestamp'
  ];
  detailProperties = [
    'batteryStatus',
    'currentMileage',
    'motorHealth',
    'brakePadCondition',
    'brakeFluidLevel',
    'tirePressureLow',
    'airConditioningFunctional',
    'softwareUpToDate',
    'timestamp'
  ];
  detailMap!: Map<string, string>;

  drivers: DriverWithId[] = [];
  driversAvailableToTrade: DriverWithId[] = [];

  selectedDriver!: DriverWithId;
  selectedTradeDriver!: number | undefined;

  expandedElement!: Vehicle | null;

  impersonationSnackbarRef!: any;

  mockTrips = [
    { origin: '34.118434,-118.300393', destination: '34.078036,-118.474095' },
    { origin: '40.7812,-73.9665', destination: '40.6892,-74.0445' },
    { origin: '41.8826,-87.6226', destination: '41.8789,-87.6359' },
    { origin: '38.2577,-85.7636', destination: '38.2029,-85.7700' },
    { origin: '34.0522,-118.2437', destination: '39.7392,-104.9903' },
    { origin: '25.7617,-80.1918', destination: '32.7767,-96.7970' },
    { origin: '33.7490,-84.3880', destination: '41.8781,-87.6298' },
    { origin: '47.6062,-122.3321', destination: '37.7749,-122.4194' }
  ];

  nextCharge!: string;

  isLoading = false;
  isRTDLoading = false;

  isRTD = true;
  isTrading = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChildren(RippleKeyDirective) rippleKeys!: QueryList<RippleKeyDirective>;
  @ViewChildren(MatExpansionPanel) panels!: QueryList<MatExpansionPanel>;

  constructor(
    private vehicleDataService: VehicleDataService,
    private snackBar: MatSnackBar,
    private loggedInService: LoggedinService
  ) {
    this.loggedInService.userValue$.subscribe((value: number) => this.user = value);
  }

  ngOnInit(): void {
    this.setRunningClock();
  }

  ngAfterViewInit(): void {
    this.columnsMap = this.initColumns(this.columnsToDisplay, this.columnProperties);
    this.columnsToDisplayWithExpand = [...this.columnsMap.values(), 'expand'];
    this.detailMap = this.initColumns(this.detailHeaders, this.detailProperties);

    this.loadDashboard();
  }

  ngOnDestroy(): void {
    if (this.timerId) {
      clearInterval(this.timerId);
    }

    this.unsubscribe(this.vehicleStaticDataSub);
    this.unsubscribe(this.vehicleRealTimeDataSub);

    if (this.user === 0) {
      this.snackBar.dismiss();
    }
  }

  /**
   * Initializes and loads the dashboard data based on the user's role.
   * If the user is an admin, the whole fleet data is fetched; otherwise, only specific vehicle data is fetched.
   *
   * @returns {void}
   */
  loadDashboard(): void {
    this.setRunningClock();
    this.isLoading = true;
    this.updatedTime = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString();

    // reset data
    this.unsubscribe(this.vehicleStaticDataSub);
    this.drivers = [];
    this.staticData = [];

    if (this.user === 0) { // 0 = admin
      this.fetchFleet();
    } else {
      this.fetchVehicle();
    }
  }

  fetchFleet(): void {
    setTimeout(() => {
      this.vehicleStaticDataSub = this.vehicleDataService.fetchAllVehicleInfo().subscribe((data: VehicleInfo[]) => {
        this.vehicleInfo = JSON.parse(JSON.stringify(data));

        data.forEach(element => {
          this.staticData.push(element.vehicle);
          let driver: DriverWithId = {
            vehicleId: element.vehicle.vehicleId,
            driverId: element.driver.driverId,
            firstName: element.driver.firstName,
            lastName: element.driver.lastName
          };
          this.drivers.push(driver);
        });
        this.drivers.sort((a, b) => {
          return a.vehicleId - b.vehicleId;
        });

        this.staticData.forEach(element => {
          element.batteryCapacity = element.batteryCapacity + ' kW';
        });
        this.staticData.sort((a, b) => {
          return a.vehicleId - b.vehicleId;
        });

        this.dataSource = new MatTableDataSource<Vehicle>(this.staticData);
        this.dataSource.paginator = this.paginator;

        this.isLoading = false;
      },
        (e: { message: any; }) => {
          this.snackBar.open("Error retrieving fleet.", "Dismiss", {
            duration: 3000,
            horizontalPosition: "center",
            verticalPosition: "bottom"
          });
          this.unsubscribe(this.vehicleStaticDataSub);
          this.isLoading = false;
        });
    }, 500);
  }

  fetchVehicle(): void {
    this.vehicleStaticDataSub = this.vehicleDataService.fetchVehicleByDriver(this.user).subscribe((data: VehicleInfo) => {
      let driver: Partial<DriverWithId> = {
        firstName: data.driver.firstName,
        lastName: data.driver.lastName
      };
      this.initializeImpersonateSnackbar(driver); // user is impersonating driver.

      this.staticData.push(data.vehicle);
      this.staticData.forEach(element => {
        element.batteryCapacity = element.batteryCapacity + ' kW';
      });

      this.dataSource = new MatTableDataSource<Vehicle>(this.staticData);
      this.dataSource.paginator = this.paginator;

      this.expandedElement = this.staticData[0];
      this.fetchRealTimeData(this.staticData[0], false);

      this.isLoading = false;
    },
      (e: { message: any; }) => {
        this.snackBar.open("Error retrieving fleet.", "Dismiss", {
          duration: 3000,
          horizontalPosition: "center",
          verticalPosition: "bottom"
        });
        this.unsubscribe(this.vehicleStaticDataSub);
        this.isLoading = false;
      });
  }

  /**
   * Fetches and processes real-time data for a given vehicle, updating the UI based on the received data.
   * If on a mobile device, expands the panel for the given vehicle. If not on a mobile device, closes all panels.
   *
   * @method
   * @name fetchRealTimeData
   * @param {Vehicle} vehicle - The vehicle for which real-time data is to be fetched.
   * @param {boolean} isMobile - Flag indicating whether the application is being viewed on a mobile device.
   * @throws {Error} Throws an error if there is an issue fetching or processing the real-time data.
   * @return {void}
   */
  fetchRealTimeData(vehicle: Vehicle, isMobile: boolean): void {
    this.isRTDLoading = true;
    if (isMobile) {
      this.expandedElement = vehicle;
    } else {
      this.panels.forEach(panel => panel.close());
    }

    if (this.expandedElement != null) {
      this.unsubscribe(this.vehicleRealTimeDataSub);
      this.selectedTradeDriver = undefined;

      // Fetch real-time data for given vehicle and poll until closed
      setTimeout(() => {
        this.vehicleRealTimeDataSub = interval(3500).pipe(
          startWith(0),
          switchMap(() => this.vehicleDataService.fetchRTD(vehicle.vehicleId))
        ).subscribe(
          (data: RealTimeData) => {
            this.processRealTimeData(data, vehicle.vehicleId);
            this.isRTDLoading = false;
            this.isRTD = true;
          },
          (e) => {
            this.unsubscribe(this.vehicleRealTimeDataSub);
            this.isRTDLoading = false;
            this.isRTD = false;
          }
        )
      }, 500);
    }
  }

  /**
   * Processes the real-time data for a specific vehicle, updates the list of available drivers for trade,
   * and determines which data points have changed since the last update.
   *
   * @private
   * @param {RealTimeData} data - The real-time data to be processed.
   * @param {number} id - The ID of the vehicle associated with the real-time data.
   * @returns {void}
   */
  private processRealTimeData(data: RealTimeData, id: number): void {
    this.driversAvailableToTrade = this.drivers ? JSON.parse(JSON.stringify(this.drivers)).filter((driver: { driverId: number; }) =>
      (driver.driverId !== this.drivers.find(driver => driver.vehicleId === id)!.driverId)) : [];

    const oldDetailDataCopy = this.detailData ? { ...this.detailData } : null;

    this.detailData = this.formatRealTimeData(data);
    const newDetailDataCopy = { ...this.detailData };

    // Timestamp is not considered when determining whether real-time data has updated.
    if (oldDetailDataCopy) {
      delete oldDetailDataCopy.timestamp;
    }
    delete newDetailDataCopy.timestamp;

    if (oldDetailDataCopy) {
      this.determineChangedData(oldDetailDataCopy, newDetailDataCopy);
    }

    this.selectedDriver = this.drivers.find(driver => driver.vehicleId === id)!;
  }

  /**
   * Initializes a map of property names to display names for columns. Used for table columns and real-time data properties.
   *
   * @method
   * @name initColumns
   * @param {string[]} displayNames - An array of display names for the columns.
   * @param {string[]} properties - An array of property names corresponding to the display names.
   * @returns {Map<string, string>} A map with property names as keys and display names as values.
   */
  initColumns(displayNames: string[], properties: string[]): Map<string, string> {
    let map = new Map<string, string>();
    let iterate = 0;

    properties.forEach(element => {
      map.set(element, displayNames[iterate]);
      iterate++;
    });
    return map;
  }

  /**
   * Trades vehicle assignments between driver of current vehicle row with user choice from dropdown.
   *
   * @method
   * @name tradeAssignment
   * @throws Will throw an error if the selectedTradeDriver is not defined, or if API response is not 200.
   * @return {void}
   */
  tradeAssignment(): void {
    // Can set to arbitrary vehicleInfo objects since API only cares about driver ids at the service level.
    let vehicleOne = JSON.parse(JSON.stringify(this.vehicleInfo[0]));
    let vehicleTwo = JSON.parse(JSON.stringify(this.vehicleInfo[0]));

    vehicleOne.driver.driverId = this.selectedDriver.driverId;
    vehicleTwo.driver.driverId = this.selectedTradeDriver!;

    this.tradeSub = this.vehicleDataService.tradeAssignments(vehicleOne, vehicleTwo).subscribe((data: VehicleInfo[]) => {
      if (data) {
        console.log(this.selectedDriver.driverId, ' -> ', this.selectedTradeDriver);
        this.selectedTradeDriver = undefined;
        this.loadDashboard();
        this.snackBar.open("Assignments were successfully traded!", "Dismiss", {
          duration: 3000,
          horizontalPosition: "center",
          verticalPosition: "bottom"
        });
        this.unsubscribe(this.tradeSub);
      }
    },
      (e) => {
        this.snackBar.open("Error trading vehicle assignments.", "Dismiss", {
          duration: 3000,
          horizontalPosition: "center",
          verticalPosition: "bottom"
        });
        this.unsubscribe(this.tradeSub);
      });
  }

  getAiInsights(vehicle: Vehicle): void {
    this.aiSub = this.vehicleDataService.fetchAiInsights(vehicle, this.detailData).subscribe((data: string) => {
      this.nextCharge = new Date(data).toLocaleDateString() + ' ' + new Date(data).toLocaleTimeString();
    }, (e: any) => {
      let message = 'Error retrieving AI Insights.';
      if (e.status === 425) {
        message = 'Not enough data for AI Insights. Please try again later.';
      }

      this.snackBar.open(message, "Dismiss", {
        duration: 3000,
        horizontalPosition: "center",
        verticalPosition: "bottom"
      });
      this.unsubscribe(this.aiSub);
    });
  }

  impersonateDriver(vehicle: Vehicle): void {
    const driver: DriverWithId = this.drivers.find(driver => driver.vehicleId === vehicle.vehicleId)!;
    this.loggedInService.setUserValue(driver.driverId);

    this.loadDashboard();
  }

  initializeImpersonateSnackbar(driver: DriverWithId | Partial<DriverWithId>): void {
    this.impersonationSnackbarRef = this.snackBar.open("Impersonating Driver: " + driver.firstName + ' ' + driver.lastName, "Stop", {
      horizontalPosition: "center",
      verticalPosition: "bottom"
    });

    this.impersonationSnackbarRef.onAction().subscribe(() => {
      this.stopImpersonatingDriver();
    });
  }

  stopImpersonatingDriver(): void {
    this.user = 0;
    this.loggedInService.setUserValue(0);

    this.unsubscribe(this.vehicleRealTimeDataSub);
    this.expandedElement = null;

    this.loadDashboard();
  }

  determineChangedData(oldDataCopy: any, newDataCopy: any): void {
    this.rippleKeys.forEach(rippleKey => {
      const key = rippleKey.key;
      if (oldDataCopy[key] !== newDataCopy[key]) {
        const ripple = rippleKey.getRippleInstance();
        this.launchRippleByInstance(ripple);
      }
    });
  }

  launchRippleByInstance(ripple: MatRipple): void {
    ripple.launch({
      centered: false,
      animation: {
        enterDuration: 200,
        exitDuration: 1600
      }
    });
  }

  setRunningClock(): void {
    this.timerId = window.setInterval(() => {
      const now = new Date();

      this.localTime = now.toLocaleTimeString('en-US', { hour12: false });
      this.utcTime = now.toISOString().substr(11, 8);
    }, 1000);
  }

  openTrip(): void {
    const randomTrip = this.mockTrips[Math.floor(Math.random() * this.mockTrips.length)];

    const url = `https://www.google.com/maps/dir/?api=1&origin=${randomTrip.origin}&destination=${randomTrip.destination}`;
    window.open(url, '_blank');
  }

  formatRealTimeData(data: RealTimeData): any {
    return {
      ...data,
      batteryStatus: `${Math.max(0, data.batteryStatus)} kW`,
      currentMileage: `${data.currentMileage.toLocaleString()} mi`,
      brakePadCondition: `${Math.max(0, data.brakePadCondition)}%`,
      brakeFluidLevel: `${Math.max(0, data.brakeFluidLevel)}%`,
      airConditioningFunctional: data.airConditioningFunctional ? 'Yes' : 'No',
      tirePressureLow: data.tirePressureLow ? 'Low PSI Detected' : 'Good',
      softwareUpToDate: data.softwareUpToDate ? 'Up-to-Date' : 'Needs Updating',
      timestamp: new Date(data.timestamp).toLocaleTimeString(),
    };
  }

  accordianClosed(): void {
    if (this.vehicleRealTimeDataSub) {
      this.unsubscribe(this.vehicleRealTimeDataSub);
    }
  }

  unsubscribe(subscriber: Subscription): void {
    if (subscriber) {
      subscriber.unsubscribe();
    }
  }

  originalOrder = (a: KeyValue<string, string>, b: KeyValue<string, string>): number => {
    return 0;
  }
}