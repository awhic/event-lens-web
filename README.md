# EasyFleet UI

## Description

An Angular-powered Single-Page Web Application designed for the administration and monitoring of autonomous, all-electric vehicle fleets.

>Note: Login credentials are "admin" and "admin" for the username and password.

## Local Installation

### Prerequisites

Before you begin, ensure you have the following installed on your machine:

- Node.js
- npm
- Angular CLI

EasyFleet UI is dependent on the Main Application to start. Ensure it is running first: https://gitlab.com/awhic/event-lens-service/-/blob/main/README.md.

### Quickstart

Start up the Main Application: https://gitlab.com/awhic/event-lens-service/-/blob/main/README.md.

You will need to navigate to src/app/service/vehicle-data.service.ts and change the baseUrl. If running services locally, uncomment the localhost baseUrl.
If you want to interact with your own openshift deployed services, refactor the baseUrl to your own host, followed by "api/v1":

![img.png](src/assets/base-url-img.png)

Run `npm install` to install dependencies.

Run `ng serve` and navigate to `http://localhost:4200/`.

## Vercel Deployment

This project was built off of a template that can be deployed to Vercel with zero configuration.
Although it is recommended to either use the [live demo](https://event-lens-web.vercel.app) or interact with openshift-deployed services locally,
you can also deploy this web app easily. Just fork it, make the base url adjustments as above, login to [Vercel](https://vercel.com/) via "Continue with GitLab", and select your forked repository.

### Notes from Vercel

Deploy your own Angular project with Vercel.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https://github.com/vercel/vercel/tree/main/examples/angular&template=angular)

_Live Example: https://angular-template.vercel.app_

## Usage

The EasyFleet UI is designed to act as the front-end interface that communicates with the EasyFleet Service via REST.
It provides a seamless and interactive user experience for the administration and monitoring of autonomous, all-electric vehicle fleets.

## Additional Resources

- [Angular](https://angular.io/)
  - [Angular CLI](https://github.com/angular/angular-cli)
  - [Angular CLI Overview and Command Reference](https://angular.io/cli)
- [Angular Material](https://material.angular.io/)
- [Karma](https://karma-runner.github.io)

## Contact

For any inquiries, please feel free to reach out to me at [awhixk@gmail.com](mailto:awhixk@gmail.com).

## License

This project is licensed under the MIT License - see [LICENSE.md](LICENSE.md).
